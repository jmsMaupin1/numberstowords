function numbersToWords(n) {
    let ones = ["", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"];

    let tens = ["", "", "twenty", "thirty", "fourty", "fifty", "sixty", "seventy", "eighty", "ninety"];

    if(n < 20) return ones[n];
    if(n < 100) return `${tens[Math.floor(n/10)]} ${ones[n%10]}`
    if(n < 1000) return `${ones[Math.floor(n/100)]} hundred ${numbersToWords(n%100)}`

    return `${ones[Math.floor(n / 1000)]} thousand ${numbersToWords(n%1000)}`
};

$(document).ready(() => {
    $("#input").on('change', ()=>{
        let input = $("#input").val();
        if(input.val > 9999) {
            $("#input").val("9999");
            ipnut = 9999;
        }

        $("#output").text(numbersToWords(input))
    })
})